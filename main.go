package main

import (
	"fmt"
	"public-ecommerce/controllers"
	"public-ecommerce/middlewares"
	"public-ecommerce/models"

	"github.com/gin-gonic/gin"
)

func main() {
	SetupServer().Run()
	fmt.Println("Server is running")
}

func SetupServer() *gin.Engine {
	r := gin.Default()
	models.Connect()

	// Static
	// r.Static("/", "./public")

	//Authentication
	r.POST("/api/register", controllers.AuthRegister)
	r.POST("/api/login", controllers.AuthLogin)

	//Product
	r.POST("/api/product", middlewares.AuthToken, controllers.StoreProduct)
	r.GET("/api/product", controllers.ListProduct)
	r.GET("/api/:store_domain/:product_slug", controllers.DetailProduct)

	//Cart
	r.POST("/api/cart", middlewares.AuthToken, controllers.StoreCart)
	r.GET("/api/cart", middlewares.AuthToken, controllers.ListCart)
	r.DELETE("/api/cart/:id", middlewares.AuthToken, controllers.DeleteCart)

	//Transaction
	r.POST("/api/transaction/create", middlewares.AuthToken, controllers.CreateTransaction)
	r.GET("/api/transaction/detail", middlewares.AuthToken, controllers.DetailTransaction)
	r.GET("/api/transaction", middlewares.AuthToken, controllers.ListTransaction)

	//Midtrans
	r.POST("/api/midtrans/check", middlewares.AuthToken)
	r.POST("/api/midtrans/notification", controllers.MidtransOnNotification)
	r.GET("/api/midtrans/finish", middlewares.AuthTokenByQuery, controllers.MidtransOnSuccessAndPending)
	r.GET("/api/midtrans/error", middlewares.AuthTokenByQuery, controllers.MidtransOnErrorAndOnClose)

	// Simple CRUD Test
	r.GET("/api/dummy", controllers.ListDummy)
	r.POST("/api/dummy", controllers.StoreDummy)
	r.POST("/api/dummy/:id", controllers.UpdateDummy)
	r.DELETE("/api/dummy/:id", controllers.DeleteDummy)
	return r
}
