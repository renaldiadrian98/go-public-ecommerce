package models

import "time"

//Handle table name
func (ProductFull) TableName() string {
	return "products"
}
func (ProductOnly) TableName() string {
	return "products"
}

type ProductFull struct {
	ID        string    `json:"id" gorm:"primary_key"`
	Name      string    `json:"name"`
	Image     string    `json:"image"`
	Price     int       `json:"price"`
	UserId    string    `json:"user_id"`
	User      User      `json:"user,omitempty" `
	Slug      string    `json:"slug"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ProductOnly struct {
	ID         string    `json:"id" gorm:"primary_key"`
	Name       string    `json:"name"`
	Image      string    `json:"image"`
	Price      int       `json:"price"`
	UserId     string    `json:"user_id"`
	Slug       string    `json:"slug"`
	CategoryId string    `json:"category_id"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

func GetProductOnly(productId string) ProductOnly {
	var product ProductOnly
	DB.Where("id = ?", productId).Find(&product)
	return product
}
