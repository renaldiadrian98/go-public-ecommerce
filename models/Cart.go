package models

import (
	"time"
)

type Cart struct {
	ID        string      `json:"id" gorm:"primary_key"`
	ProductId string      `json:"product_id"`
	Product   ProductOnly `json:"product"`
	UserId    string      `json:"user_id"`
	User      User        `json:"user"`
	Quantity  int         `json:"quantity"`
	CreatedAt time.Time   `json:"created_at"`
	UpdatedAt time.Time   `json:"updated_at"`
}

func CheckExistingCart(productId string, userId string) bool {
	var cart []Cart
	DB.Where("product_id = ? AND user_id = ?", productId, userId).Find(&cart)
	if len(cart) > 0 {
		return true
	}
	return false
}

func GetActiveCart(userId string) []Cart {
	var cart []Cart
	DB.Where("user_id = ?", userId).Find(&cart)
	for i := 0; i < len(cart); i++ {
		cart[i].Product = GetProductOnly(cart[i].ProductId)
	}
	return cart
}
