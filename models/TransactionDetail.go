package models

import "time"

type TransactionDetail struct {
	ID            string      `json:"id" gorm:"primary_key"`
	ProductId     string      `json:"product_id"`
	Product       ProductOnly `json:"product"`
	Quantity      int         `json:"quantity"`
	Price         int         `json:"price"`
	SubTotal      int         `json:"sub_total"`
	TransactionId string      `json:"transaction_id"`
	CreatedAt     time.Time   `json:"created_at"`
	UpdatedAt     time.Time   `json:"updated_at"`
}

func GetTransactionDetail(transactionId string, userId string) ([]TransactionDetail, error){
	var transactionDetail []TransactionDetail
	err := DB.Raw(`
		SELECT td.*
		FROM transaction_details td
		JOIN transactions t
		on t.id = td.transaction_id
		WHERE
		t.user_id = ?
		AND
		t.id = ?
	`, userId, transactionId).Scan(&transactionDetail).Error
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(transactionDetail); i++ {
		transactionDetail[i].Product = GetProductOnly(transactionDetail[i].ProductId)
	}
	return transactionDetail, nil
}