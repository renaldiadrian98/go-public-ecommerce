package models

import (
	"time"
)

type Dummy struct {
	ID          string `json:"id" gorm:"primary_key"`
	Description string `json:"description"`
	Image       string `json:"image"`
	imageUrl    string
	Number      int       `json:"number"`
	UserId      string    `json:"user_id"`
	User        User      `json:"user"`
	CreatedAt   time.Time `json:"created_at"`
	UpdatedAt   time.Time `json:"updated_at"`
}

func (d *Dummy) SetImageUrl() {
	if d.Image != "" {
		d.imageUrl = "/file/" + d.Image
	}
	d.imageUrl = ""
}
