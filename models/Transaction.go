package models

import "time"

//Handle table name
func (TransacationFull) TableName() string {
	return "transactions"
}
func (TransacationWithTransactionDetail) TableName() string {
	return "transactions"
}
func (Transaction) TableName() string {
	return "transactions"
}

type Transaction struct {
	ID               string    `json:"id" gorm:"primary_key"`
	UserId           string    `json:"user_id"`
	Total            int       `json:"total"`
	Status           string    `json:"status"`
	PaymentGatewayId string    `json:"payment_gateway_id"`
	Invoice          string    `json:"invoice"`
	CreatedAt        time.Time `json:"created_at"`
	UpdatedAt        time.Time `json:"updated_at"`
}

type TransacationWithTransactionDetail struct {
	ID                string              `json:"id" gorm:"primary_key"`
	UserId            string              `json:"user_id"`
	Total             int                 `json:"total"`
	Status            string              `json:"status"`
	TransactionDetail []TransactionDetail `json:"transaction_detail"`
	PaymentGatewayId  string              `json:"payment_gateway_id"`
	CreatedAt         time.Time           `json:"created_at"`
	UpdatedAt         time.Time           `json:"updated_at"`
}

type TransacationFull struct {
	ID                string              `json:"id" gorm:"primary_key"`
	UserId            string              `json:"user_id"`
	Total             int                 `json:"total"`
	Status            string              `json:"status"`
	TransactionDetail []TransactionDetail `json:"transaction_detail"`
	PaymentGatewayId  string              `json:"payment_gateway_id"`
	PaymentGateway    PaymentGateway      `json:"payment_gateway"`
	CreatedAt         time.Time           `json:"created_at"`
	UpdatedAt         time.Time           `json:"updated_at"`
}

type TransactionWithMidtransSnapToken struct {
	Transaction       Transaction `json:"transaction"`
	MidtransSnapToken string      `json:"midtrans_snap_token"`
}

func ValidateTransactionStatus(status string) (string, bool) {
	if status == "waiting" || status == "pending" ||status == "expired" || status == "paid" || status == "sent" || status == "received" || status == "failed" {
		return status, true
	}
	return "", false
}
