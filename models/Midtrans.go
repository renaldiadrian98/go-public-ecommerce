package models

import (
	"encoding/json"
	"net/http"
	"os"
	"time"

	"github.com/joho/godotenv"
	gomidtrans "github.com/veritrans/go-midtrans"
)

type Midtrans struct {
	ID            string    `json:"id" gorm:"primary_key"`
	TransactionId string    `json:"transaction_id"`
	Status        string    `json:"status"`
	PaymentType   string    `json:"payment_type"`
	SignatureKey  string    `json:"signature_key"`
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
}

var midclient gomidtrans.Client
var CoreGateway gomidtrans.CoreGateway
var snapGateway gomidtrans.SnapGateway

func SetupMidtrans() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	midclient := gomidtrans.NewClient()
	midclient.ServerKey = os.Getenv("MIDTRANS_SERVER_KEY")
	midclient.ClientKey = os.Getenv("MIDTRANS_CLIENT_KEY")
	midclient.APIEnvType = gomidtrans.Sandbox
	CoreGateway = gomidtrans.CoreGateway{
		Client: midclient,
	}

	snapGateway = gomidtrans.SnapGateway{
		Client: midclient,
	}
}

func GenerateSnapMidtrans(userId string, total int64, transactionId string) (string, error) {
	SetupMidtrans()

	//Customer Data
	var user User
	DB.Where("id = ?", userId).Find(&user)

	custAddress := &gomidtrans.CustAddress{
		FName:       user.Name,
		LName:       "",
		Phone:       "081234567890",
		Address:     "Baker Street 97th",
		City:        "Jakarta",
		Postcode:    "16000",
		CountryCode: "IDN",
	}

	//Append items
	transactionDetails, _ := GetTransactionDetail(transactionId, userId)
	var snapReq gomidtrans.SnapReq
	snapReq.Items = new([]gomidtrans.ItemDetail)
	for i := 0; i < len(transactionDetails); i++ {
		*snapReq.Items = append(*snapReq.Items, gomidtrans.ItemDetail{
			ID:    transactionDetails[i].ProductId,
			Price: int64(transactionDetails[i].Product.Price),
			Qty:   int32(transactionDetails[i].Quantity),
			Name:  transactionDetails[i].Product.Name,
		})
	}

	snopReq := &gomidtrans.SnapReq{
		TransactionDetails: gomidtrans.TransactionDetails{
			OrderID:  transactionId,
			GrossAmt: total,
		},
		CustomerDetail: &gomidtrans.CustDetail{
			FName:    user.Name,
			LName:    "",
			Email:    user.Email,
			Phone:    "081234567890",
			BillAddr: custAddress,
			ShipAddr: custAddress,
		},
		Items: snapReq.Items,
	}

	snapTokenResp, err := snapGateway.GetToken(snopReq)
	if err != nil {
		return "", err
	}
	return snapTokenResp.Token, nil
}

func MidtransCheck(transactionid string) (map[string]interface{}, error) {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	serverKey := os.Getenv("MIDTRANS_SERVER_KEY_BASE64")
	targetUrl := "https://api.sandbox.midtrans.com/v2/" + transactionid + "/status"
	req, err := http.NewRequest("GET", targetUrl, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", serverKey)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	var data map[string]interface{}
	json.NewDecoder(resp.Body).Decode(&data)
	return data, nil
}
