package models

type User struct {
	ID          string `json:"id" gorm:"primary_key"`
	Email       string `json:"email"`
	Name        string `json:"name"`
	Password    string `json:"password"`
	StoreName   string `json:"store_name"`
	StoreDomain string `json:"store_domain"`
}

func CheckExistingUser(email string) (User, error) {
	var user User
	err := DB.Where("email = ?", email).First(&user).Error
	if err != nil {
		return user, err
	}
	return user, nil
}

func CheckEmail(email string) error {
	var user User
	err := DB.Where("email = ?", email).First(&user).Error
	if err != nil {
		return err
	}
	return nil
}

func GetUser(userId string) User{
	var user User
	DB.Where("id = ?", userId).Find(&user)
	return user
}
