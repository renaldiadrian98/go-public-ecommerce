CREATE TABLE IF NOT EXISTS transaction_details(
   id VARCHAR (255) PRIMARY KEY,
   product_id VARCHAR (255) NOT NULL,
   quantity INT NOT NULL,
   price INT NOT NULL,
   sub_total INT NOT NULL,
   transaction_id VARCHAR(255) NOT NULL,
   created_at TIMESTAMP NOT NULL DEFAULT NOW(),
   updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);