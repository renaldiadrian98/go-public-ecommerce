CREATE TABLE IF NOT EXISTS payment_gateways(
   id VARCHAR (255) PRIMARY KEY,
   name VARCHAR (255) NOT NULL,
   created_at TIMESTAMP NOT NULL DEFAULT NOW(),
   updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

INSERT INTO payment_gateways (id, name, created_at, updated_at)
VALUES('1','Midtrans', NOW(), NOW());

INSERT INTO payment_gateways (id, name, created_at, updated_at)
VALUES('2','Doku', NOW(), NOW());

ALTER TABLE transactions ADD COLUMN payment_gateway_id VARCHAR(255) NOT NULL;