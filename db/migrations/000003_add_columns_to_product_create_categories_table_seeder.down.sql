ALTER TABLE products 
DROP COLUMN created_at;
ALTER TABLE products 
DROP COLUMN updated_at;

ALTER TABLE users 
DROP COLUMN created_at;
ALTER TABLE users 
DROP COLUMN updated_at;

DROP TABLE IF EXISTS categories;

