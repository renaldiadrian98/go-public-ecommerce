package controllers

import (
	"fmt"
	"net/http"
	"public-ecommerce/helpers"
	"public-ecommerce/models"
	"time"

	"github.com/gin-gonic/gin"
)

func MidtransOnSuccessAndPending(c *gin.Context) {
	//Update to pending
	var input struct {
		OrderId string `json:"order_id"`
	}
	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}

	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)

	var transaction models.Transaction
	models.DB.Where("id = ? AND user_id = ?", input.OrderId, userIdStr).First(&transaction)
	transaction.Status = "pending"
	transaction.UpdatedAt = time.Now()
	models.DB.Save(&transaction)

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    transaction,
	})
	return
}

func MidtransOnErrorAndOnClose(c *gin.Context) {
	//Return transaction to cart
	//Frontend send transaction_id
	orderId := c.Query("order_id")

	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)

	transactionDetail, err := models.GetTransactionDetail(orderId, userIdStr)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Not Found",
			"error":   err.Error(),
		})
		return
	}

	//Foreach Transaction Details To Cart and Delete Transaction Details
	for _, item := range transactionDetail {
		var cart models.Cart
		cart.ID = helpers.GenerateID()
		cart.ProductId = item.ProductId
		cart.Quantity = item.Quantity
		cart.UserId = userIdStr
		cart.CreatedAt = item.CreatedAt
		cart.UpdatedAt = item.UpdatedAt
		models.DB.Save(&cart)
		models.DB.Delete(&item)
	}
	var transactionDetailSingular models.TransactionDetail
	models.DB.Where("id = ?", orderId).Delete(&transactionDetailSingular)

	//delete transaction
	var transaction models.Transaction
	models.DB.Where("id = ?", orderId).Delete(&transaction)

	c.JSONP(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    nil,
	})
	return
}

func MidtransOnNotification(c *gin.Context) {
	var input struct {
		OrderId string `json:"order_id" binding:"required"`
	}
	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}

	resultArray, err := models.MidtransCheck(input.OrderId)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Failed Checking Status",
			"error":   err.Error(),
		})
		return
	}

	//Handle Response
	var transaction models.Transaction
	models.DB.Where("id = ?", resultArray["order_id"]).First(&transaction)
	if resultArray["transaction_status"] == "capture" || resultArray["transaction_status"] == "settlement" {
		transaction.Status = "paid"

	} else if resultArray["transaction_status"] == "pending" {
		transaction.Status = "pending"
	} else {
		transaction.Status = "failed"
	}
	transaction.UpdatedAt = time.Now()
	models.DB.Save(&transaction)

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    transaction,
	})
	return
}
