package controllers

import (
	"fmt"
	"net/http"
	"public-ecommerce/helpers"
	"public-ecommerce/models"
	"time"

	"github.com/gin-gonic/gin"
)

func DetailTransaction(c *gin.Context) {
	var transaction models.TransacationWithTransactionDetail
	transactionId := c.Query("id")
	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)

	err := models.DB.Where("user_id = ? AND id = ?", userIdStr, transactionId).Find(&transaction).Error
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "Not Found",
			"error":   nil,
		})
		return
	}

	transaction.TransactionDetail, err = models.GetTransactionDetail(transactionId, userIdStr)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "Error Getting Transaction Detail",
			"error":   err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    transaction,
	})
	return
}

func ListTransaction(c *gin.Context) {
	var transaction []models.Transaction
	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)

	err := models.DB.Where("user_id = ?", userIdStr).Find(&transaction).Error
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "Not Found",
			"error":   nil,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    transaction,
	})
	return
}

func CreateTransaction(c *gin.Context) {
	var input struct {
		PaymentGatewayId string `json:"payment_gateway_id" binding:"required"`
	}
	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}
	//Move cart to transaction detail and transaction
	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)
	cart := models.GetActiveCart(userIdStr)
	var transaction models.Transaction
	transaction.ID = helpers.GenerateID()
	transaction.UserId = userIdStr
	transaction.PaymentGatewayId = input.PaymentGatewayId
	transaction.Invoice = helpers.CreateInvoice(transaction.ID)
	transactionStatus, isValid := models.ValidateTransactionStatus("waiting")
	if isValid == false {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"message": "Failed Saving Data",
			"error":   "Invalid Status",
		})
		return
	}
	total := 0
	for _, item := range cart {
		var transactionDetail models.TransactionDetail
		transactionDetail.ID = helpers.GenerateID()
		transactionDetail.ProductId = item.ProductId
		transactionDetail.Quantity = item.Quantity
		transactionDetail.Price = item.Product.Price
		transactionDetail.SubTotal = item.Product.Price * item.Quantity
		transactionDetail.TransactionId = transaction.ID
		transactionDetail.CreatedAt = time.Now()
		transactionDetail.UpdatedAt = time.Now()
		total = total + transactionDetail.SubTotal
		models.DB.Save(&transactionDetail)
	}
	transaction.Total = total
	transaction.Status = transactionStatus
	transaction.CreatedAt = time.Now()
	transaction.UpdatedAt = time.Now()

	//Generate Midtrans Token If Payment Gateway Is Midtrans
	var snapToken string
	var snapError error
	if input.PaymentGatewayId == "1" {
		snapToken, snapError = models.GenerateSnapMidtrans(userIdStr, int64(total), transaction.ID)
		if snapError != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"success": false,
				"message": "Failed Generating Midtrans Snap Token",
				"error":   snapError.Error(),
			})
			return
		}
	}

	models.DB.Save(&transaction)
	var transactionData models.TransactionWithMidtransSnapToken
	transactionData.Transaction = transaction
	transactionData.MidtransSnapToken = snapToken

	//Delete active cart
	var deleteCart models.Cart
	models.DB.Where("user_id = ?", userIdStr).Delete(&deleteCart)

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    transactionData,
	})
	return
}
