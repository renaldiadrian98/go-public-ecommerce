package controllers

import (
	"net/http"
	"public-ecommerce/helpers"
	"public-ecommerce/models"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

func AuthLogin(c *gin.Context) {
	var input struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
	}

	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}

	//Check if user exist
	user, err := models.CheckExistingUser(input.Email)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "User Not Found",
			"error":   err.Error(),
		})
		return
	}

	//Check if password is wrong
	errpwd := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(input.Password))
	if errpwd != nil && errpwd == bcrypt.ErrMismatchedHashAndPassword {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid Password",
			"error":   errpwd.Error(),
		})
		return
	}

	token, err := helpers.Encrypt(user.Email)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Failed creating token",
			"error":   err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"success": true,
		"message": "Login Success",
		"data":    user,
		"token":   token,
	})
	return
}

func AuthRegister(c *gin.Context) {
	var user models.User
	var input struct {
		Email    string `json:"email" binding:"required"`
		Password string `json:"password" binding:"required"`
		Name     string `json:"name" binding:"required"`
	}

	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}

	//Check for duplicate register
	isExist, err := models.CheckExistingUser(input.Email)
	if isExist.Email != "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "User Already Registered",
			"error":   "Duplicate Data",
		})
		return
	}

	pass, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Failed Creating Password",
			"error":   err.Error(),
		})
		return
	}

	user.Password = string(pass)
	user.ID = helpers.GenerateID()
	user.Name = input.Name
	user.Email = input.Email
	models.DB.Save(&user)
	token, err := helpers.Encrypt(input.Email)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Failed creating token",
			"error":   err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"success": true,
		"message": "Account Created",
		"data":    user,
		"token":   token,
	})
	return

}
