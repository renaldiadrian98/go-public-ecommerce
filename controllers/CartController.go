package controllers

import (
	"fmt"
	"net/http"
	"public-ecommerce/helpers"
	"public-ecommerce/models"

	"github.com/gin-gonic/gin"
)

func StoreCart(c *gin.Context) {
	var input struct {
		ProductId string `json:"product_id" binding:"required"`
		Quantity  int    `json:"quantity"`
	}
	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}
	if input.Quantity < 1 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   nil,
		})
		return
	}

	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)

	//Check duplicate cart
	isDuplicate := models.CheckExistingCart(input.ProductId, userIdStr)
	if isDuplicate == true {
		var cart models.Cart
		models.DB.Preload("Product").Preload("User").Where("product_id = ? AND user_id = ?", input.ProductId, userId).First(&cart)
		cart.Quantity = input.Quantity
		if input.Quantity < 1 {
			err := decrementCart(cart.ID, userIdStr)
			if err != nil {
				c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
					"success": false,
					"message": "Failed Deleting Cart",
					"error":   nil,
				})
				return
			}
			c.JSON(http.StatusCreated, gin.H{
				"success": true,
				"message": "Deleted",
				"data":    nil,
			})
			return
		} else {
			models.DB.Save(&cart)
		}

		c.JSON(http.StatusCreated, gin.H{
			"success": true,
			"message": "Success",
			"data":    cart,
		})
		return
	} else {
		var cart models.Cart
		cart.ID = helpers.GenerateID()
		cart.ProductId = input.ProductId
		cart.Quantity = input.Quantity
		cart.UserId = userIdStr
		err := models.DB.Save(&cart).Error
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"success": false,
				"message": "Failed Saving Data",
				"error":   err.Error(),
			})
			return
		}

		//Re-query to preload model association
		var addedCart models.Cart
		models.DB.Preload("Product").Preload("User").Where("id = ?", cart.ID).Find(&addedCart)
		c.JSON(http.StatusCreated, gin.H{
			"success": true,
			"message": "Success",
			"data":    addedCart,
		})
		return
	}

}

func ListCart(c *gin.Context) {
	// Manual without preload
	// userId := c.MustGet("userId")
	// userIdStr := fmt.Sprintf("%v", userId)

	// cart := models.GetActiveCart(userIdStr)
	// c.JSON(http.StatusOK, gin.H{
	// 	"success": true,
	// 	"message": "Success",
	// 	"data":    cart,
	// })
	// return

	var cart []models.Cart

	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)

	err := models.DB.Preload("Product").Preload("User").Where("user_id = ?", userIdStr).Find(&cart).Error
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "Not Found",
			"error":   nil,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    cart,
	})
	return

}

func DeleteCart(c *gin.Context) {
	userId := c.MustGet("userId")
	userIdStr := fmt.Sprintf("%v", userId)
	err := decrementCart(c.Param("id"), userIdStr)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Failed Deleting Cart",
			"error":   nil,
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"success": false,
		"message": "Success",
		"error":   nil,
	})
	return
}

func decrementCart(cartId string, userId string) error {
	var cart models.Cart
	err := models.DB.Where("id = ? AND user_id = ?", cartId, userId).Delete(&cart).Error
	if err != nil {
		return err
	}
	return nil
}
