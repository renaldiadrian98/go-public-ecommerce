package controllers

import (
	"net/http"
	"path/filepath"
	"public-ecommerce/helpers"
	"public-ecommerce/models"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func StoreDummy(c *gin.Context) {
	file, err := c.FormFile("image")
	var newFileName string
	if err == nil {
		extension := filepath.Ext(file.Filename)
		newFileName = uuid.New().String() + extension
	}

	var input struct {
		Description string `form:"description"`
		Number      int    `form:"number"`
	}
	if err := c.ShouldBind(&input); err != nil {
		c.AbortWithStatusJSON(500, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}

	// userId := c.MustGet("userId")
	// userIdStr := fmt.Sprintf("%v", userId)

	var dummy models.Dummy
	dummy.ID = helpers.GenerateID()
	dummy.Description = input.Description
	dummy.Image = newFileName
	dummy.Number = input.Number
	dummy.UserId = "1"
	err = models.DB.Save(&dummy).Error
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{
			"success": false,
			"message": "Failed Saving Data",
			"error":   err.Error(),
		})
		return
	}

	//Re-query to preload model association
	var addedDummy models.Dummy
	addedDummy.SetImageUrl()
	models.DB.Preload("User").Where("id = ?", dummy.ID).Find(&addedDummy)
	c.JSON(http.StatusCreated, gin.H{
		"success": true,
		"message": "Success",
		"data":    addedDummy,
	})
	return
}

func ListDummy(c *gin.Context) {
	// Manual without preload
	// userId := c.MustGet("userId")
	// userIdStr := fmt.Sprintf("%v", userId)

	// cart := models.GetActiveCart(userIdStr)
	// c.JSON(http.StatusOK, gin.H{
	// 	"success": true,
	// 	"message": "Success",
	// 	"data":    cart,
	// })
	// return

	var dummies []models.Dummy

	err := models.DB.Preload("User").Find(&dummies).Error
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "Not Found",
			"error":   nil,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    dummies,
	})
	return
}

func UpdateDummy(c *gin.Context) {
	id := c.Param("id")
	var input struct {
		Description string `json:"product_id"`
		Number      int    `json:"number"`
	}
	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(500, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}

	var dummy models.Dummy
	models.DB.Where("id = ?", id).First(&dummy)
	dummy.Description = input.Description
	dummy.Number = input.Number
	models.DB.Save(&dummy)

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    dummy,
	})
	return
}

func DeleteDummy(c *gin.Context) {
	id := c.Param("id")
	var dummy models.Dummy
	err := models.DB.Where("id = ?", id).Delete(&dummy).Error
	if err != nil {
		c.AbortWithStatusJSON(500, gin.H{
			"success": false,
			"message": "Failed Deleting Dummy",
			"error":   nil,
		})
		return
	}
	c.JSON(201, gin.H{
		"success": false,
		"message": "Success",
		"error":   nil,
	})
	return
}
