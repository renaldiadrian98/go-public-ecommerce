package controllers

import (
	"fmt"
	"net/http"
	"path/filepath"
	"public-ecommerce/helpers"
	"public-ecommerce/models"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func DetailProduct(c *gin.Context) {
	storeDomain := c.Param("store_domain")
	productSlug := c.Param("product_slug")
	var product models.ProductFull
	err := models.DB.Raw(`
		SELECT P.*
		FROM products P
		JOIN users U
		on P.user_id = U.id
		WHERE
		U.store_domain = ?
		AND
		P.slug = ?
	`, storeDomain, productSlug).Scan(&product).Error
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"success": false,
			"message": "Not Found",
			"error":   nil,
		})
		return
	}
	//Add relational data
	product.User = models.GetUser(product.UserId)
	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"message": "Success",
		"data":    product,
	})
	return
}

func ListProduct(c *gin.Context) {
	q := c.Query("q")
	orderBy := strings.ToLower(c.Query("order_by"))
	sortBy := strings.ToLower(c.Query("sort_by"))
	productQuery := models.DB
	var product []models.ProductOnly
	if q != "" {
		productQuery = productQuery.Where("name ILIKE ?", "%"+q+"%")
	}
	if orderBy != "" {
		if sortBy != "" {
			productQuery = productQuery.Order(orderBy + " " + sortBy)
		} else {
			productQuery = productQuery.Order(orderBy)
		}
	}
	errQuery := productQuery.Find(&product).Error
	if errQuery != nil || len(product) == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Not Found",
			"error":   nil,
		})
		return
	}
	c.JSON(http.StatusCreated, gin.H{
		"success": true,
		"message": "Success",
		"data":    product,
	})
	return
}

func StoreProduct(c *gin.Context) {
	file, err := c.FormFile("image")
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "File is Required",
			"error":   err.Error(),
		})
		return
	}
	extension := filepath.Ext(file.Filename)
	newFileName := uuid.New().String() + extension

	var input struct {
		Name  string `form:"name" binding:"required"`
		Price int    `form:"price" binding:"required"`
	}
	if err := c.ShouldBind(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Invalid input",
			"error":   err.Error(),
		})
		return
	}

	//Check for duplication slug
	var findProduct models.ProductOnly
	slug := helpers.CreateSlug(input.Name)
	userId := c.MustGet("userId")
	result := models.DB.Where("slug = ? AND user_id = ?", slug, userId).First(&findProduct)
	if result.RowsAffected >= 1 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "You Already Have That Product",
			"error":   "Duplicate Data",
		})
		return
	}

	//Handle upload file
	if err := c.SaveUploadedFile(file, "public/img/products/"+newFileName); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"message": "Unable to save the file",
			"error":   err.Error(),
		})
		return
	}

	userIdStr := fmt.Sprintf("%v", userId)

	var product models.ProductOnly
	product.ID = helpers.GenerateID()
	product.Name = input.Name
	product.Image = newFileName
	product.Price = input.Price
	product.UserId = userIdStr
	product.Slug = slug
	product.CreatedAt = time.Now()
	product.UpdatedAt = time.Now()
	errResult := models.DB.Save(&product).Error
	if errResult != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"success": false,
			"message": "Failed Saving Data",
			"error":   errResult.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"success": true,
		"message": "Success",
		"data":    product,
	})
	return
}
