package middlewares

import (
	"net/http"
	"net/mail"
	"public-ecommerce/helpers"
	"public-ecommerce/models"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func AuthTokenByQuery(c *gin.Context) {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	token := c.Query("token")
	email, err := helpers.Decrypt(token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Decrypt Failed",
			"error":   err.Error(),
		})
		return
	}
	//Despite success on decrypting, it may return symbol
	_, errDecrypt := mail.ParseAddress(email)
	if errDecrypt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "User Not Found",
			"error":   "Auth Failure",
		})
		return
	}
	user, errUser := models.CheckExistingUser(email)
	if errUser != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "User Not Found",
			"error":   err.Error(),
		})
		return
	}
	// log.Println("email ",email)
	c.Set("userId", user.ID)
	c.Next()
}

func AuthToken(c *gin.Context) {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	const BEARER_SCHEMA = "Bearer "
	authHeader := c.GetHeader("Authorization")
	if authHeader == "" {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "No Authorization Header",
			"error":   "No authorization",
		})
		return
	}

	arrAuth := strings.Fields(authHeader)
	token := arrAuth[1]

	email, err := helpers.Decrypt(token)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "Decrypt Failed",
			"error":   err.Error(),
		})
		return
	}
	//Despite success on decrypting, it may return symbol
	_, errDecrypt := mail.ParseAddress(email)
	if errDecrypt != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "User Not Found",
			"error":   "Auth Failure",
		})
		return
	}
	user, errUser := models.CheckExistingUser(email)
	if errUser != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"success": false,
			"message": "User Not Found",
			"error":   err.Error(),
		})
		return
	}
	// log.Println("email ",email)
	c.Set("userId", user.ID)
	c.Next()
}
